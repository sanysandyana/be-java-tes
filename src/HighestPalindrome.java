public class HighestPalindrome {

    public static String highestPalindrome(String s, int k) {
        char[] chars = s.toCharArray();
        char[] result = highestPalindromeHelper(chars, k, 0, chars.length - 1);
        return result != null ? new String(result) : "-1";
    }

    private static char[] highestPalindromeHelper(char[] s, int k, int left, int right) {
        if (left >= right) {
            return k >= 0 ? s : null;
        }

        if (s[left] != s[right]) {
            if (k <= 0) {
                return null;
            }
            char maxChar = (char) Math.max(s[left], s[right]);
            s[left] = maxChar;
            s[right] = maxChar;
            return highestPalindromeHelper(s, k - 1, left + 1, right - 1);
        } else {
            return highestPalindromeHelper(s, k, left + 1, right - 1);
        }
    }

    public static void main(String[] args) {
        String s1 = "3943";
        int k1 = 1;
        System.out.println("Sampel 1 => "+highestPalindrome(s1, k1));

        String s2 = "932239";
        int k2 = 2;
        System.out.println("Sampel 1 => "+highestPalindrome(s2, k2));
    }
}
