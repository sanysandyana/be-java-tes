import java.util.Stack;

public class BalancedBrackets {
    public static String isBalanced(String s) {
        Stack<Character> stack = new Stack<>();

        for (char c : s.toCharArray()) {
            if (c == '{' || c == '[' || c == '(') {
                stack.push(c);
            } else if (c == '}' || c == ']' || c == ')') {
                if (stack.isEmpty()) {
                    return "NO";
                }
                char top = stack.pop();
                if ((c == '}' && top != '{') ||
                        (c == ']' && top != '[') ||
                        (c == ')' && top != '(')) {
                    return "NO";
                }
            }
        }
        return stack.isEmpty() ? "YES" : "NO";
    }

    public static void main(String[] args) {
        String sample1 = "{ [ ( ) ] }";
        String sample2 = "{ [ ( ] ) }";
        String sample3 = "{ ( ( [ ] ) [ ] ) [ ] }";

        System.out.println("Sampel 1 => "+isBalanced(sample1));
        System.out.println("Sampel 2 => "+isBalanced(sample2));
        System.out.println("Sampel 3 => "+isBalanced(sample3));
    }
}
