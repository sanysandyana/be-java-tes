import java.util.HashSet;
import java.util.Set;

public class WeightedStrings {

    public static int calculateWeight(char c) {
        return c - 'a' + 1;
    }

    public static Set<Integer> getAllWeights(String s) {
        Set<Integer> weights = new HashSet<>();
        int i = 0;

        while (i < s.length()) {
            char charAtI = s.charAt(i);
            int weight = calculateWeight(charAtI);
            weights.add(weight);

            int count = 1;
            while (i + 1 < s.length() && s.charAt(i + 1) == charAtI) {
                count++;
                i++;
                weights.add(weight * count);
            }
            i++;
        }

        return weights;
    }

    public static String[] weightedStrings(String s, int[] queries) {
        Set<Integer> weights = getAllWeights(s);
        String[] result = new String[queries.length];

        for (int i = 0; i < queries.length; i++) {
            if (weights.contains(queries[i])) {
                result[i] = "Yes";
            } else {
                result[i] = "No";
            }
        }

        return result;
    }

    public static void main(String[] args) {
        String s = "abbcccd";
        int[] queries = {1, 3, 9, 8};
        String[] results = weightedStrings(s, queries);

        for (String result : results) {
            System.out.println(result);
        }
    }
}
